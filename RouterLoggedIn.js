import React, {Component} from 'react';
import {createStackNavigator, createBottomTabNavigator, createAppContainer} from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from './Router2'
import ProfileScreen from './Profile'
import AboutScreen from './About'
const AppNavigator = createBottomTabNavigator(
    {
        Home: HomeScreen, 
        Profile: ProfileScreen,
        About: AboutScreen
    },//ios-information
    {
      defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home`;
        } else if (routeName === 'Profile') {
          iconName = `ios-person`;
        }
        else if(routeName=='About'){
          iconName ='ios-information'
        }
        
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    
      tabBarOptions: {
      activeTintColor: '#f85a40',
      inactiveTintColor: 'gray',
        }
        
    },
    {
        InitialRouteName: "Home",
        
        defaultNavigationOptions: {
        headerStyle:{
            backgroundColor: '#ddd'
            },
        headerTintColor: '#000',
        headerTitleStyle:{
            fontWeight:'bold'    
            }
        }
    }
)
/*const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component{
    render(){
        return <AppContainer />;
    }
} */
export default createStackNavigator({ AppNavigator},{ headerMode: "none" });
