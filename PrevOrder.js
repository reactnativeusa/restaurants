import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, FlatList, ScrollView,Image,ImageBackground , TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';


export default class PrevOrder extends React.Component {
    constructor(props){
        super(props)
        this.getdata();
        this.state = {
            data:''
        }

    }  
    
    static navigationOptions = {
        title:"Previous orders"
    }

    getdata= async ()=>{
        let previousOrders = await AsyncStorage.getItem('@previous_orders');        
        previousOrders= JSON.parse(previousOrders);
        this.setState({data:previousOrders})
        console.log("getData: "+JSON.stringify(previousOrders))
    }
    eraseData = () => {
        try {
          AsyncStorage.clear(()=>{
              console.warn("data cleared")
              this.setState({data:''})
          })
        } 
        catch(e) {
            console.warn('Error Erasing Data: ', e)
        }
    }
    renderItem = ({ item, index }) => {
        if (item.empty === true) {
    
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
     
    return (
        <TouchableOpacity style={styles.item}>
            <Text style={styles.itemText}>{"Order : "+item.title+"\n" + "Type : "+item.name+"\n" + "Price : " +item.price}</Text>
        </TouchableOpacity>
    );
  };
render(){
    return(
    <View>
    {
     (this.state.data==null) ? 
     <View style={styles.headlineView}><Text style={styles.headline}>You have no previous orders</Text></View>
     :
     <ScrollView>
         <View style={styles.button}><Button  title='Clear History' onPress={this.eraseData} /></View>
     <FlatList
            data={this.state.data}
            style={styles.container}
            renderItem={this.renderItem} />
     </ScrollView>
    }
    </View>
    )
}



}
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        marginVertical: 20,
                 },
item:{
    padding:10,
    flex: 1,
    margin:8,
    borderBottomWidth:0.5

    
}, 
  itemText: {
    color: '#2b8415',
    fontSize:15,
    fontWeight:'bold'
    },

    itemInvisible: {
        backgroundColor: 'transparent',
      },

      button:{
        fontSize:25,
        fontWeight:'bold',
        margin:25,
       
      },headline: {
            textAlign: 'center', 
            fontWeight: 'bold',
            fontSize: 18,
    },headlineView: {
            paddingTop:300,
            justifyContent: 'center', 
            alignItems: 'center'
    }


           
})