import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, FlatList, Dimensions,Image,ImageBackground , TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation';


export default class CategoryList extends React.PureComponent {
    constructor(props){
        super(props);

        this.state={
            data:''
        }
    }
  static navigationOptions = {
    header: null
  }
 
  onPressItem=(item)=>{
    this.props.navigation.navigate('ItemDetails',{item:item}); 
  }

  renderItem = ({ item, index }) => {
        if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
        <TouchableOpacity style={styles.item} onPress={() => this.onPressItem(item)}>
            <Image style={styles.image} source={item.img} ></Image><Text style={styles.itemText}>{item.type}</Text>
        </TouchableOpacity>
    );
  };

 render() {
  const { navigation } = this.props;
  const propsData = navigation.getParam('data', 'NODATA');
  this.setState({data:propsData})
  
 return (
       
         <FlatList
            data={this.state.data}
            style={styles.container}
            renderItem={this.renderItem} 
             />

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
      marginTop:70
  },
  item: {
    padding:2,
    flex: 1,
    margin: 0.3,
      borderWidth:0.3,
      borderColor:'grey',
             flexDirection:'row'

  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#2b8415',
      fontSize:18,
      marginTop:50,
      fontWeight:'bold',
      marginLeft:10
  },
    image:{
       margin: 10,
         alignItems: 'center',
    justifyContent: 'center',
     
        width:100,
        height:100,
        borderRadius: 20 ,
 
    },
    
 
});