import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';

import RouterScreen from "./RouterLoggedIn";
import SplashScreen from "./SplashScreen";

const MainNavigator = createStackNavigator({
  Splash: {screen: SplashScreen},
  Router: {screen: RouterScreen}
  },
  {headerMode: 'none'}
);
const App = createAppContainer(MainNavigator);

export default App ;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
