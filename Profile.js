import React from "react";
import { View,ScrollView, Text, Button, TextInput,StyleSheet} from "react-native";
import { Card} from 'react-native-elements'

let userInfo;
export default class Profile extends React.Component {
   constructor(props) {
    super(props);
    userInfo={fullname:"Safa Deldoul", email:"safa@gmail.com", address:"3835 Green Pond Rd", phone:"4842547896",
                card:"1234518795",expiration:"05/21", csv:"451", zip:"18020"};
    this.state = {
      text: '',
      editingMode:false,
      //userInfoAux:userInfo
      fullnameAux:userInfo.fullname,
      emailAux:userInfo.email,
      addressAux:userInfo.address,
      phoneAux:userInfo.phone,
      cardAux:userInfo.card,
      expirationAux:userInfo.expiration,
      csvAux:userInfo.csv,
      zipAux:userInfo.zip
    };
  }
    static navigationOptions = {
    title: 'Profile', header: null
   };

  onSaved=()=>{
    this.setState({editingMode:false});
      userInfo={fullname:this.state.fullnameAux, email:this.state.emailAux, address:this.state.addressAux, phone:this.state.phoneAux,
         card:this.state.cardAux, expiration:this.state.expirationAux,
      csv:this.state.csvAux, zip:this.state.zipAux }
    
   }

  onCanceled=()=>{
     this.setState({editingMode:false, fullnameAux:userInfo.fullname, emailAux:userInfo.email, addressAux:userInfo.address,
      phoneAux:userInfo.phone, cardAux:userInfo.card, expirationAux:userInfo.expiration,
      csvAux:userInfo.csv, zipAux:userInfo.zip})
   }
  render() {
    
    if (!this.state.editingMode)
    {return (
       <ScrollView>
        <Card title="Personal information">
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>First Name </Text><Text style={[styles.text1]}> {userInfo.fullname} </Text></View>      
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Email Address </Text><Text style={[styles.text1]}>{userInfo.email}</Text></View>            
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Address </Text><Text style={[styles.text1]}>{userInfo.address}</Text></View>   
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Phone # </Text><Text style={[styles.text1]}>{userInfo.phone}</Text></View>
         </Card>

         <Card title="Paiment information">
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Card # </Text><Text style={[styles.text1]}>{userInfo.card}</Text></View>
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Expiration </Text><Text style={[styles.text1]}>{userInfo.expiration}</Text></View>
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>CSV </Text><Text style={[styles.text1]}>{userInfo.csv}</Text></View>
            <View style={[styles.rowContainer]}><Text style={[styles.text]}>Zip Code </Text><Text style={[styles.text1]}>{userInfo.zip}</Text></View>
         </Card>
         <View style={[styles.button]}><Button  title="Edit" onPress={()=>this.setState({editingMode:true})}/></View>
        </ScrollView>
    )} else 
    {return (
      <ScrollView>
       <Card title="Personal information">
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Full name </Text><TextInput
          value={this.state.fullnameAux} placeholder="Please enter your name"
          onChangeText={(text) => this.setState({fullnameAux:text})}/></View> 
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Email address </Text><TextInput
          value={this.state.emailAux} placeholder="Please enter your email"
          onChangeText={(text) => this.setState({emailAux:text})}/></View>            
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Address </Text><TextInput
          value={this.state.addressAux} placeholder="Please enter your address"
          onChangeText={(text) => this.setState({addressAux:text})}/></View>   
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Phone # </Text><TextInput
          value={this.state.phoneAux} placeholder="Please enter your phone number"
          onChangeText={(text) => this.setState({phoneAux:text})}/></View>
        </Card>

        <Card title="Paiment information">
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Card # </Text><TextInput
          value={this.state.cardAux} placeholder="Please enter your card number"
          onChangeText={(text) => this.setState({cardAux:text})}/></View>
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Expiration </Text><TextInput
          value={this.state.expirationAux} placeholder="Please enter your expiration date"
          onChangeText={(text) => this.setState({expirationAux:text})}/></View>
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>CSV </Text><TextInput
          value={this.state.csvAux} placeholder="Please enter your CSV code"
          onChangeText={(text) => this.setState({csvAux:text})}/></View>
           <View style={[styles.rowContainer]}><Text style={[styles.text]}>Zip Code </Text><TextInput
          value={this.state.zipAux} placeholder="Please enter your Zip code"
          onChangeText={(text) => this.setState({zipAux:text})}/></View>
        </Card>
        <View style={[styles.button]}>
            <Button  title="Save" onPress={this.onSaved}/>
            <Button  title="Cancel" onPress={this.onCanceled} color="#f85a40" />
        </View>
       </ScrollView>
   )}
  }
}
const styles = StyleSheet.create({
  
  button:{
    fontSize:25,
    fontWeight:'bold',
    margin: 15
  },
    text:{
      color:"#037ef3",
      fontSize:20,
      margin: 15
    },
    text1:{
        fontSize:18,
        fontWeight:'bold',
        margin: 15
    },
   rowContainer:{
       flexDirection:'row'
   }
})