import React from "react";
import { View,ScrollView, Text, Button, TextInput,StyleSheet, Image,TouchableHighlight,Linking} from "react-native";
import { Card} from 'react-native-elements';
let userInfo;
export default class AboutScreen extends React.Component {
    onpress=() =>{
        console.warn("pic pressed")
        Linking.openURL('https://www.facebook.com/nycvillagepizza/').catch(err => console.error('An error occurred', err));
    }
render(){

    return(
        <ScrollView >
         <Card title='Address' style={styles.container}>
        <Text style={styles.text1}>129 W.4th Street, Bethlehem,PA</Text>
        <Image source={require('./images/map.png')} style={styles.image}></Image>
        </Card>
         <Card title='Business Hours'>
         <Text style={styles.text1}>Monday : Closed</Text>
         <Text style={styles.text1}>Tuesday: 11:00 AM-10:00 PM</Text>
         <Text style={styles.text1}>Wednesday: 11:00 AM-10:00 PM</Text>
         <Text style={styles.text1}>Thursday: 11:00 AM-10:00 PM</Text>
         <Text style={styles.text1}>Friday: 11:00 AM-11:00 PM</Text>
         <Text style={styles.text1}>Saturday: 11:00 AM-11:00 PM</Text>
         <Text style={styles.text1}>Sunday: 11:00 AM-9:00 PM</Text>
         </Card>
         <Card title='Contact'>
         <Text style={styles.text}>Contact Us : </Text>
         <Text style={styles.text1}>610-419-8799</Text>
            <TouchableHighlight onPress={this.onpress} style={styles.image1}>
                <Image source={require('./images/fb.jpg')} style={styles.image1} />
            </TouchableHighlight>
         </Card>
        </ScrollView>
    )
   
}


}
const styles = StyleSheet.create({
    container: {
      

      
    },
    text:{
        color:"#037ef3",
      fontSize:24,
      margin: 15,
      fontWeight:'bold'

    },
    text1:{
       
      fontSize:20,
      margin: 15,
      fontWeight:'bold'

    },
    image:{
        width:350,
        height:350,
        
        
    },
    image1:{
        width:70,
        height:70,
        margin:10
    }
   });