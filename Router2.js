import { createStackNavigator } from 'react-navigation'
import HomeScreen from './HomeScreen';
import CategoryList from './CategoryList';
import ItemDetails from './ItemDetails';
import PrevOrder from './PrevOrder'
export default createStackNavigator({
    HomeScreen,
    CategoryList,
    ItemDetails,
    PrevOrder
});