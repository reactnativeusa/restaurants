import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, ImageBackground} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import Router from "./RouterLoggedIn";

export default class SplashScreen extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            timePassed: false,
        };
    }
    
    static navigationOptions = {
      header: null
    }
    
    componentDidMount() {
        setTimeout( () => {
            this.setTimePassed();
        },1000);
    }

    setTimePassed() {
        this.setState({timePassed: true});
        this.props.navigation.navigate('Router');
    }
    

    render() {
        return(
                <ImageBackground source={require( './images/splashScreen.png')} style={{width: '100%', height: '100%'}}>
               
                </ImageBackground>
               
        )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch' <Image source={require('splashScreen.png')} style={styles.backgroundImage} />
      //    style={{width: 100, height: 100}}
        //  source={{uri: 'https://drive.google.com/file/d/1Lq9u615B2dCzf8zBd_W7SawnvHR3E5y5/view?usp=sharing'}}
  }
});

