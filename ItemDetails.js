import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {Platform, StyleSheet, Text, View, Picker,Image,ScrollView,Alert} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import { Card, Button } from 'react-native-elements'
console.UnableYellowBox = true;
export default class ItemDetails extends React.PureComponent {
     

    constructor(props){
        super(props);
        const { navigation } = this.props;  
        const propsData = navigation.getParam('item', 'NODATA');
        this.state = {
            details:propsData.details,
            title:propsData.type,
            choice:propsData.details.types[0].name,
            price:propsData.details.types[0].price
        };
    }

    static navigationOptions = {
        header:null
    }
  
    choiceChanged = (itemValue, itemIndex) =>{
        this.setState({choice: itemValue})
        if(itemIndex==0){ 
            this.setState({price:this.state.details.types[0].price})
        }else{
            this.setState({price:this.state.details.types[1].price})
        }
    } 

    order= async ()=>{
        
            const item = {title:this.state.title, name:this.state.choice, price: this.state.price }; 
            try {
                
                let previousOrders = await AsyncStorage.getItem('@previous_orders');
                previousOrders= JSON.parse(previousOrders);

                if (previousOrders!=null)
                {
                    try {
                    
                    previousOrders.push(item)
                    await AsyncStorage.setItem('@previous_orders', JSON.stringify(previousOrders));
                    Alert.alert("We'll be preparing your order soon!",item.title,
                    [{text: 'OK', onPress: () => console.log('OK Pressed')},],
                   {cancelable: false},);
                    console.log('Saved previousOrders: ', previousOrders)
                        }
                    catch (e) {
                        console.log('Error Saving Data: ', e)
                              } 
                }else{
                    let data=[item];
                    await AsyncStorage.setItem('@previous_orders', JSON.stringify(data));
                  Alert.alert("We'll be preparing your order soon!",item.title,
                    [{text: 'OK', onPress: () => console.log('OK Pressed')},],
                   {cancelable: false},);
                     }
                
                 }catch (e) {
                console.log('Error Saving Data: ', e)
                            }
      
                       }

    render() { 
            return(
                <ScrollView>
                    <Card title={this.state.title}>
                        <Text style={styles.desc}>{this.state.details.description}</Text>
                    </Card>
                    <Card> 
                        <Image source={this.state.details.img} style={styles.image}></Image>
                    </Card>
                    
                    {
                        (this.state.details.types[1]==null)?
                        <View style={styles.container}>
                        <Text style={styles.textname}>{this.state.details.types[0].name}</Text>
                        <Button title={"Order for "+this.state.details.types[0].price}onPress={this.order}/> 
                        </View>
                        :
                        <View style={styles.container}>
                        <Picker 
                        selectedValue={this.state.choice}
                        style={{height: 100, width: 300}}
                        onValueChange={this.choiceChanged}>
                        <Picker.Item label={this.state.details.types[0].name} value={this.state.details.types[0].name} />
                        <Picker.Item label={this.state.details.types[1].name} value={this.state.details.types[1].name} />
                        </Picker> 
                        <Button title={"Order for "+this.state.price} style={styles.button} onPress={this.order}/>
                        </View>                    
                    }
                    
                </ScrollView>    
            )
  
   }
}


const styles = StyleSheet.create({ 
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      image: {
        width: null,
        resizeMode: 'contain',
        height: 220
    },
    desc:{
      fontSize:20
    },
    textname:{
        fontSize:15,
        textAlign: 'center',
        margin:10
    },
    button:{
        marginTop:100
    }

})