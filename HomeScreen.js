import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, FlatList, Dimensions,Image,ScrollView , TouchableOpacity} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import data from './appData'



 const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }
  return data;
};

 const numColumns = 3;
console.disableYellowBox = true; 


export default class HomeScreen extends React.Component {
  constructor (props){
    super(props),
    this.onPress = this.onPressItem.bind(this);
  }
  static navigationOptions = {
    title:"Home"
  }
  onPressItem=(item)=>{
      this.props.navigation.navigate('CategoryList',{data:item.content}); 
  }
  prevorder=() =>{
    this.props.navigation.navigate('PrevOrder');
  }

  renderItem = ({ item, index }) => {
     

    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
     
        <TouchableOpacity style={styles.item} onPress={() => this.onPressItem(item)}>
            <Image style={styles.image} source={item.img} ></Image>
            <Text style={styles.itemText}>{item.type}</Text>
        </TouchableOpacity>
        
    );
  };

 render() {

 return (
       <ScrollView>
         <View style={styles.button} ><Button title='Previous Orders' onPress={this.prevorder} /></View>
         <FlatList
            data={formatData(data, numColumns)}style={styles.container}
            renderItem={this.renderItem}
            numColumns={numColumns} />
          </ScrollView>

        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, //numColumns={numColumns}
    marginVertical: 20,
             },
  item: {
    padding:3,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 0.7,
    height: Dimensions.get('window').width / numColumns, // approximate a square
    borderWidth:0.7,
    borderColor:'grey'
       },
  itemInvisible: {
    backgroundColor: 'transparent',
                 },
  itemText: {
    color: '#2b8415',
    fontSize:15,
    fontWeight:'bold'
           },
    image:{
       margin: 10,
       alignItems: 'center',
       justifyContent: 'center',
       flex: 1,  
       width:Dimensions.get('window').width/1.3 / numColumns,
       height: Dimensions.get('window').width/1.3 / numColumns
          },
          button:{
            fontSize:25,
            fontWeight:'bold',
            margin:25,
           
          
          },
          

});