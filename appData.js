const data = [
    {
        type: 'Entrees', 
        img:require('./images/entrees.jpg'),
        content:[
         { type: 'Chicken Kebab over rice or Fries', 
          img:require('./images/chickk.jpg'),
          details: {
              description:"Lean chuncs of chicken, tenderized and marinated, skewered, and grilled over charcoal." ,
              img:require('./images/chickk.jpg') ,
              types:[
                  {
                  name:'Chicken Kebab over rice or Fries',
                  price:"$12.95"
                  }
              ]
           }
          
          },
          { type: 'Shish Kebab over rice or Fries', 
          img:require('./images/shishk.jpg'),
          details: {
              description:"Tenderloin cubes of lamb, marinated with our chefs unique seasoning, grilled over charcoal." ,
              img:require('./images/shishk.jpg') ,
              types:[
                  {
                  name:"Shish Kebab over rice or Fries",
                  price:"$14.95"
                  }
                  
              ]
           }
          
          },
          { type: 'Adana Kebab over rice or fries', 
          img:require('./images/adk.jpg'),
          details: {
              description:"Lamb flavored with red bell peppers, slightly seasoned with paprika and grilled over charcoal." ,
              img:require('./images/adk.jpg') ,
              types:[
                  {
                  name:"Adana Kebab over rice or fries",
                  price:"$13.95"
                  }
              ]
           }
          
          },
          { type: 'Gyro/Doner over rice or Fries', 
          img:require('./images/egyro.jpg'),
          details: {
              description:"Vertically grilled and marinated thin steak (beef and lamb), sliced thinly." ,
              img:require('./images/egyro.jpg') ,
              types:[
                  {
                  name:"Gyro/Doner over rice or  Fries",
                  price:"$13.95"
                  }
                   ]
           }
          
          },
          { type: 'Mixed Grill (Entrees 8 thru 11)', 
          img:require('./images/mixed.jpg'),
          details: {
              description:"Mixed Grill containing chicken Kebab, Shish Kebab, Adana Kebab and Gyro over rice or French Fries." ,
              img:require('./images/mixed.jpg') ,
              types:[
                  {
                  name:"Mixed Grill (Entrees 8 thru 11)",
                  price:"$19.95"
                  }
                   ]
           }
          
          }
        ]
        },

         {
      type: 'Sandwiches ', 
      img:require('./images/sandw.jpg'),
      content:[
       { type: 'Gyro/Döner', 
        img:require('./images/gyro.jpg'),
        details: {
            description:"Lamb, tomato and onion concoction nestled in a fold of a soft bread." ,
            img:require('./images/gyro.jpg') ,
            types:[
                {
                name:"Pita",
                price:"$7.25"
                },
                {
                name:"Wrap",
                price:"$7.99"
                }
            ]
         }
        
        },
     {
      type: 'Chicken Kebab',
      img:require('./images/kebab.jpg'),
      details:{
          description:"Chicken Breast, Romaine Lettuce, Feta Cheese,Bread, Onions, Red Ripe Tomatoes",
          img:require('./images/kebab.jpg'),
          types:[
              {
                  name:"Pita",
                  price:"$6.75"
              },
              {
                  name:"Wrap",
                  price:"$7.25"
              }
                ]
            }
    },
      {
       type: 'Adana Kebab', 
       img:require('./images/adana.jpg'),
       details:{
            description:" Sandwich with a long, hand-minced meat kebab mounted on a wide iron skewer and grilled on an open mangal filled with burning charcoal, minced onion, minced garlic, tomatoes,red pepper ",
            img:require('./images/adana.jpg'),
            types:[
              {
                name:"Pita",
                price:"$7.25"
              },
              {
                name:"Wrap",
                price:"$7.99"
              }
                  ]
               }
      },
      { 
       type:'Shish Kebab' ,
       img:require('./images/chich.jpg'),
       details:{
        description:"Beef, lamb or chicken sandwich with onion, cherry tomatoes,pepper, mayonnaise ",
        img:require('./images/chich.jpg'),
        types:[
          {
            name:"Pita",
            price:"$7.50"
          },
          {
            name:"Wrap",
            price:"$8.49"
          }
              ]
           }
        },
      {
      type: 'Falafel',
      img:require('./images/falafel.jpg'),
      details:{
        description:"Mixture of ground chickpeas and broad beans, tossed in spices and deep fried",
        img:require('./images/falafel.jpg'),
        types:[
          {
            name:"Pita",
            price:"$6.00"
          },
          {
            name:"Wrap",
            price:"$6.99"
          }
              ]
           }
      
      }
    ]
},
{
    type: 'Pizza',
    img:require('./images/pizza.jpg'),
    content:[
       {
        type:"Plain pizza",
        img:require('./images/plain.jpeg'),
        details:{
            description:"Tomatoe sauce, cheese. ",
            img:require('./images/plain.jpeg'),
            types:[ 
                {
                    name:'Medium 14"',
                    price:"$10.49"
                },
                {
                    name:'Large 16"',
                    price:"$12.49"
                }
                 ]
        }
       },
       {
         type:"Sicilian pizza",
         img:require('./images/sicilian.jpg'),
         details:{
             description:"Thick crust, topped with onions, tomatoes, herbs and cheese",
             img:require('./images/sicilian.jpg'),
             type:[
                 {
                     name:'Large 16"',
                     price:"12.99"
                 }
                  ]
         }
       },
       {
        type:"White pizza",
        img:require('./images/white.jpg'),
        details:{
            description:"Ricotta, Mozzarella, Parmigiania cheese, fresh garlic, olive oil",
            img:require('./images/white.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$11.99"
                },
                {
                    name:'Large 16"',
                    price:"12.99"
                }
                 ]
        }
       },
       {
        type:"Supreme",
        img:require('./images/sup.jpg'),
        details:{
            description:"Choice of 6 toppings with Mozzarella and Sauce.",
            img:require('./images/sup.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$16.99"
                },
                {
                    name:'Large 16"',
                    price:"$18.99"
                }
                 ]
        }
      },
      {
        type:"Vegetable Pizza",
        img:require('./images/vegp.jpg'),
        details:{
            description:"Fresh Mushrooms and Tomatoes, Onions, Green Peppers, Black Olives, Spinach, Broccoli, Mozzarella Cheese and Sauce.",
            img:require('./images/vegp.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$16.99"
                },
                {
                    name:'Large 16"',
                    price:"$18.99"
                }
                 ]
        }
      },
      {
        type:"Caprese",
        img:require('./images/cap.jpg'),
        details:{
            description:"Fresh Tomatoes, Fresh Mozzarella, Fresh Garlic and Basil **NO SAUCE**.",
            img:require('./images/cap.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$15.49"
                },
                {
                    name:'Large 16"',
                    price:"$17.49"
                }
                 ]
        }
      },
      {
        type:"Hawaiian",
        img:require('./images/haw.jpg'),
        details:{
            description:"Turkey Ham, Pineapple, Mozzarella and Sauce.",
            img:require('./images/haw.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$15.49"
                },
                {
                    name:'Large 16"',
                    price:"$17.49"
                }
                 ]
        }
      },
      {
        type:"Buffalo Chicken",
        img:require('./images/buff.jpg'),
        details:{
            description:"Spicy Buffalo Chicken Breast, Mozzarella Cheese and Sauce.",
            img:require('./images/buff.jpg'),
            type:[
                {
                    name:'Medium 14"',
                    price:"$15.49"
                },
                {
                    name:'Large 16"',
                    price:"$17.49"
                }
                 ]
        }
      },
      



    ]
},
    {
     type: 'Stromboli', 
     img:require('./images/stromboli.png'),
     content:[
        {
            type:"Steak stromboli",
            img:require('./images/steakstromboli.jpg'),
            details:{
                description:"Steak, Cheese, Side Sauce. ",
                img:require('./images/steakstromboli.jpg'),
                types:[ 
                    {
                        name:'Small',
                        price:"$8.99"
                    },
                    {
                        name:'Large',
                        price:"$13.99"
                    }
                     ]
            }
        },
        {
            type:"Meat Stromboli",
            img:require('./images/meatstromboli.jpg'),
            details:{
                description:"Beef, Pepperoni, Beef sausage, Meatballs, Cheese and Side sauce",
                img:require('./images/meatstromboli.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$8.99"

                    },
                    {
                        name:"Large",
                        price:"$13.99"
                    }
                      ]
                     }
        },
        {
            type:"Vegetable Stromboli",
            img:require('./images/vegetablestromboli.jpg'),
            details:{
                description:"Fresh mushrooms, Green peppers, Broccoli, Cheese and Side sauce.",
                img:require('./images/vegetablestromboli.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$8.99"

                    },
                    {
                        name:"Large",
                        price:"$13.99"
                    }
                      ]
                     }
        },
        {
            type:"Gyro Stromboli",
            img:require('./images/gyrostromboli.jpg'),
            details:{
                description:"Gyro meat and cheese.",
                img:require('./images/gyrostromboli.jpg'),
                types:[
                    
                    {
                        name:"Large",
                        price:"$18.99"
                    }
                      ]
                     }
        }
                ]
    },
    { 
     type:'Burgers' ,
     img:require('./images/burger.jpg'),
     content:[
        {
            type:"Hamburger Plain",
            img:require('./images/hamburgerplain.jpg'),
            details:{
                description:"Bun + meat ",
                img:require('./images/hamburgerplain.jpg'),
                types:[
                    
                    {
                        name:"Plain no fries",
                        price:"$3.49"
                    },
                    {
                        name:"Deluxe with fries ",
                        price:"$5.99"

                    }
                      ]
                     }
        }, 
        {
            type:"Cheeseburger Plain",
            img:require('./images/cheeseburgerplain.jpg'),
            details:{
                description:"Bun + meat + cheese ",
                img:require('./images/cheeseburgerplain.jpg'),
                types:[
                    
                    {
                        name:"Plain no fries",
                        price:"$3.49"
                    },
                    {
                        name:"Deluxe with fries ",
                        price:"$5.99"

                    }
                      ]
                     }
        },
        {
            type:"California Cheeseburger ",
            img:require('./images/calcheeseburger.jpg'),
            details:{
                description:"Bun + meat + cheese + lettuce + tomatoes + onion + mayo ",
                img:require('./images/calcheeseburger.jpg'),
                types:[
                    
                    {
                        name:"Plain no fries",
                        price:"$4.49"
                    },
                    {
                        name:"Deluxe with fries ",
                        price:"$6.99"

                    }
                      ]
                     }


        },
        {
            type:"Grilled Chicken Breast on a bun ",
            img:require('./images/chickenburger.jpg'),
            details:{
                description:"Bun + chicken  + lettuce + tomatoes + onion + mayo ",
                img:require('./images/chickenburger.jpg'),
                types:[
                    
                    {
                        name:"Plain no fries",
                        price:"$4.49"
                    },
                    {
                        name:"Deluxe with fries ",
                        price:"$6.49"

                    }
                      ]
                     }
        },
        {
            type:"Homemade George's Hamburger ",
            img:require('./images/homemadeburger.jpg'),
            details:{
                description:"Bun + meat + lettuce + tomatoes + onion + mayo+ Provolone Cheese ",
                img:require('./images/homemadeburger.jpg'),
                types:[
                    
                    {
                        name:"Plain no fries",
                        price:"$5.99"
                    },
                    {
                        name:"Deluxe with fries ",
                        price:"$7.99"

                    }
                      ]
                    }
        }
         ]
    },
    {
    type: 'Wings',
    img:require('./images/wings.jpg'),
    content:[
        {
            type:"Mild-Medium-Hot-BBQ",
            img:require('./images/hotwings.jpg'),
            details:{
                description:"",
                img:require('./images/hotwings.jpg'),
                types:[
                    
                    {
                        name:"10 Wings",
                        price:"$9.49"
                    },
                    {
                        name:"20 Wings ",
                        price:"$16.99"

                    },
                    {
                        name:"40 Wings ",
                        price:"$31.99"

                    }

                      ]
                    }
        }
           ]
    },
    { type: 'Salads' ,
     img:require('./images/salad.jpg'),
     content:[
        {
            type:"Tossed salad",
            img:require('./images/tossed.jpeg'),
            details:{
                description:"Lettuce, Tomatoes, Onion, Green peppers, Cucumber, Red Cabbage, Carrots.",
                img:require('./images/tossed.jpeg'),
                types:[
                    {
                        name:"Small",
                        price:"$4.49"

                    },
                    {
                        name:"Large",
                        price:"$5.49"
                    }
                      ]
                     }
        },
        {
            type:"Ceasar salad",
            img:require('./images/ceasar.jpg'),
            details:{
                description:"Romaine Lettuce, Croutons, Grated Parmigiana Cheese, Tomatoes.",
                img:require('./images/ceasar.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$4.49"

                    },
                    {
                        name:"Large",
                        price:"$5.49"
                    }
                      ]
                     }
        },
        {
            type:"Greek salad",
            img:require('./images/greek.jpg'),
            details:{
                description:"Tossed salad with Anchovies, Grape leaves and Feta cheese.",
                img:require('./images/greek.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$6.49"

                    },
                    {
                        name:"Large",
                        price:"$8.49"
                    }
                      ]
                     }
        },
        {
            type:"Grilled chicken salad",
            img:require('./images/grilled.jpg'),
            details:{
                description:"Tossed salad with Grilled chicken.",
                img:require('./images/grilled.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$5.99"

                    },
                    {
                        name:"Large",
                        price:"$8.49"
                    }
                      ]
                     }
        },
        {
            type:"Fresh mozzarella Salad",
            img:require('./images/fresh.jpg'),
            details:{
                description:"Romaine Lettuce, Fresh Mozzarella, Fresh Tomatoes, Roasted red peppers.",
                img:require('./images/fresh.jpg'),
                types:[
                    {
                        name:"Small",
                        price:"$6.49"

                    },
                    {
                        name:"Large",
                        price:"$8.49"
                    }
                      ]
                     }
        },
            ]
    },
    {
    type: 'Calzone & Rolls',
    img:require('./images/calzone.jpg'),
    content:[
        {
            type:"Cheese Calzone",
            img:require('./images/cheesecal.png'),
            details:{
                description:"Pizza dough filled with Ricotta and Mozzarella Cheese.",
                img:require('./images/cheesecal.png'),
                types:[
                    {
                        name:"Normal",
                        price:"$5.49"

                    },
                    {
                        name:"With Extra Topping",
                        price:"$6.49"
                    }
                      ]
                     }
        },
        {
            type:"Sausage Roll",
            img:require('./images/sausageroll.jpg'),
            details:{
                description:"Pizza dough rolled with Beef Sausage, Green peppers and Mozzarella Cheese.",
                img:require('./images/sausageroll.jpg'),
                types:[
                    {
                        name:"Normal",
                        price:"$5.49"

                    },
                    {
                        name:"With Extra Topping",
                        price:"$6.49"
                    }
                      ]
                     }
        },
        {
            type:"Pepperoni Roll",
            img:require('./images/peproll.jpg'),
            details:{
                description:"Pizza dough rolled with Beef pepperoni, Green peppers and Mozzarella Cheese.",
                img:require('./images/peproll.jpg'),
                types:[
                    {
                        name:"Normal",
                        price:"$5.49"

                    },
                    {
                        name:"With Extra Topping",
                        price:"$6.49"
                    }
                      ]
                     }
        },
        {
            type:"Spinach Roll",
            img:require('./images/spinroll.jpg'),
            details:{
                description:"Pizza dough rolled with Spinach and Mozzarella Cheese.",
                img:require('./images/spinroll.jpg'),
                types:[
                    {
                        name:"Normal",
                        price:"$5.49"

                    },
                    {
                        name:"With Extra Topping",
                        price:"$6.49"
                    }
                      ]
                     }
        },
        {
            type:"Chicken Roll",
            img:require('./images/chickroll.jpg'),
            details:{
                description:"Pizza dough rolled with chicken and Mozzarella Cheese.",
                img:require('./images/chickroll.jpg'),
                types:[
                    {
                        name:"Normal",
                        price:"$5.49"

                    },
                    {
                        name:"With Extra Topping",
                        price:"$6.49"
                    }
                      ]
                     }
        }
           ]
    },
    {
    type: 'Cheesesteaks & Hot heroes 12" ',
    img:require('./images/cheesteaks.jpg'),
    content:[
        {
            type:"Plain Cheesesteak",
            img:require('./images/plainchee.jpg'),
            details:{
                description:"Thinly sliced pieces of beefsteak and melted cheese in a long hoagie roll. ",
                img:require('./images/plainchee.jpg'),
                types:[
                    {
                        name:"Plain Cheeseteak",
                        price:"$6.99"

                    },
                   ]
                     }
        },
        {
            type:"Philly Cheesesteak",
            img:require('./images/phillych.jpg'),
            details:{
                description:"Steak, Green Peppers, Onion, American Cheese and Sauce. ",
                img:require('./images/phillych.jpg'),
                types:[
                    {
                        name:"Philly Cheesesteak",
                        price:"$7.49"

                    },
                      ]
                     }
        },
        {
            type:"Plain chicken Cheesesteak",
            img:require('./images/chickench.jpg'),
            details:{
                description:"Chicken, melted cheese. ",
                img:require('./images/chickench.jpg'),
                types:[
                    {
                        name:"Plain chicken Cheesesteak",
                        price:"$6.99"

                    },
                      ]
                     }
        },
        {
            type:"Philly chicken Cheesesteak",
            img:require('./images/chickenph.jpg'),
            details:{
                description:"Chicken, Green Peppers,Onion, American cheese. ",
                img:require('./images/chickenph.jpg'),
                types:[
                    {
                        name:"Plain chicken Cheesesteak",
                        price:"$7.49"

                    },
                      ]
                     }
        },
        {
            type:"Meatball Hero",
            img:require('./images/meatballh.jpg'),
            details:{
                description:"Crusty rolls filled with meatballs. ",
                img:require('./images/meatballh.jpg'),
                types:[
                    {
                        name:"With Sauce",
                        price:"$6.49"

                    },
                    {
                        name:"With Cheese",
                        price:"$6.99"

                    }
                      ]
                     }
        },
        {
            type:"Eggplant Hero",
            img:require('./images/eggh.jpg'),
            details:{
                description:"Crusty rolls filled with eggplant. ",
                img:require('./images/eggh.jpg'),
                types:[
                    {
                        name:"With Sauce",
                        price:"$6.49"

                    },
                    {
                        name:"With Cheese",
                        price:"$6.99"

                    }
                      ]
                     }
        },
        {
            type:"Beef sausage Hero",
            img:require('./images/beefh.jpg'),
            details:{
                description:"Crusty rolls filled with beef sausage. ",
                img:require('./images/beefh.jpg'),
                types:[
                    {
                        name:"With Sauce",
                        price:"$6.49"

                    },
                    {
                        name:"With Cheese",
                        price:"$6.99"

                    }
                      ]
                     }
        }
            ]
    }, 
    { type: 'Pasta',
     img:require('./images/pasta.jpg'),
     content:[
        {
            type:"Spaghetti / Ziti",
            img:require('./images/spag.jpg'),
            details:{
                description:"Added Meatballs, Sausage, Meat Sauce. ",
                img:require('./images/spag.jpg'),
                types:[
                    {
                        name:"Spaghetti / Ziti",
                        price:"$11.99"

                    },
                   
                      ]
                     }
        },
        {
            type:"Chicken Parmigiana",
            img:require('./images/chp.jpg'),
            details:{
                description:"Lightly Breaded Chicken With Sauce and Cheese, served with Spaghetti or ziti. ",
                img:require('./images/chp.jpg'),
                types:[
                    {
                        name:"Chicken Parmigiana",
                        price:"$11.99"

                    },
                   
                      ]
                     }
        },
        {
            type:"Eggplant Parmigiana",
            img:require('./images/eggp.jpg'),
            details:{
                description:"Eggplant fried and baked with Sauce and Cheese, Served with Spaghetti or Ziti. ",
                img:require('./images/chp.jpg'),
                types:[
                    {
                        name:"Eggplant Parmigiana",
                        price:"$11.99"

                    },
                   
                      ]
                     }
        },
        {
            type:"Eggplant Rollatini",
            img:require('./images/eggr.jpg'),
            details:{
                description:"Eggplant stuffed with Ricotta and Mozzarella Cheese, Served With Spaghetti or Ziti. ",
                img:require('./images/eggr.jpg'),
                types:[
                    {
                        name:"Eggplant Rollatini",
                        price:"$12.99"

                    },
                   
                      ]
                     }
        }
        
             ]
    }, 
    { type: 'Soups', 
     img:require('./images/soup.jpg'),
     content:[
        {
            type:"Chicken Noodle",
            img:require('./images/chickn.jpg'),
            details:{
                description:"Chicken, Noodle, vegetables. ",
                img:require('./images/chickn.jpg'),
                types:[
                    {
                        name:"Chicken Noodle",
                        price:"$3.95"

                    },
                   
                      ]
                     }
        },
        {
            type:"Minestrone",
            img:require('./images/min.jpg'),
            details:{
                description:"thick soup made with pasta, beans, onions, celery, carrots, stock and tomatoes. ",
                img:require('./images/min.jpg'),
                types:[
                    {
                        name:"Minestrone",
                        price:"$3.95"

                    },
                   
                      ]
                     }
        },
        {
            type:"Lentil",
            img:require('./images/lent.jpg'),
            details:{
                description:"soup based on lentils made with vegetables. ",
                img:require('./images/lent.jpg'),
                types:[
                    {
                        name:"Lentil",
                        price:"$3.95"

                    }
                   
                      ]
                     }
        }


             ]
    },
    {
        type: 'Wraps & Hoagies', 
        img:require('./images/hoagies.jpg'),
        content:[
            {
                type:"Chicken Wrap",
                img:require('./images/chwrap.jpg'),
                details:{
                    description:"Grilled Chicken, Lettuce, Tomatoes, Onion, Mayo. ",
                    img:require('./images/chwrap.jpg'),
                    types:[
                        {
                            name:"Chicken Wrap",
                            price:"$6.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Gyro Wrap",
                img:require('./images/gwrap.jpg'),
                details:{
                    description:"Gyro meat, Lettuce, Tomatoes and Onion. ",
                    img:require('./images/gwrap.jpg'),
                    types:[
                        {
                            name:"Gyro Wrap",
                            price:"$7.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Steak Wrap",
                img:require('./images/swrap.jpg'),
                details:{
                    description:"Steak, Lettuce, Tomatoes, Onion and Mayo. ",
                    img:require('./images/swrap.jpg'),
                    types:[
                        {
                            name:"Steak Wrap",
                            price:"$6.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Vegetable Wrap",
                img:require('./images/vwrap.jpg'),
                details:{
                    description:"Lettuce, Tomatoes, Onion, Green peppers, Black Olives and Mayo. ",
                    img:require('./images/vwrap.jpg'),
                    types:[
                        {
                            name:"Vegetable Wrap",
                            price:"$6.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Italian Hoagie",
                img:require('./images/ihoagie.jpg'),
                details:{
                    description:"Lettuce, Tomato, Onion, Turkey Ham, Beef Salami, Provolone Cheese and Oil & Vinegar or Mayo. ",
                    img:require('./images/ihoagie.jpg'),
                    types:[
                        {
                            name:"Italian Hoagie",
                            price:"$7.49"
    
                        }
                       
                          ]
                         }
            }

                ]

    
    },
    {
        type: 'Beverages', 
        img:require('./images/beverages.jpg'),
        content:[
            {
                type:"20 oz. Bottle",
                img:require('./images/20oz.jpg'),
                details:{
                    description:"Coca Cola/ Diet Coke/ Sprite/ Fanta/ Pepsi . ",
                    img:require('./images/20oz.jpg'),
                    types:[
                        {
                            name:"20 oz. Bottle",
                            price:"$1.89"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"2 Liter Soda",
                img:require('./images/2lit.jpg'),
                details:{
                    description:"Coca Cola/ Diet Coke/ Sprite/ Fanta/ Dr pepper/ Pepsi . ",
                    img:require('./images/2lit.jpg'),
                    types:[
                        {
                            name:"2 Liter Soda",
                            price:"$2.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Water",
                img:require('./images/wat.jpg'),
                details:{
                    description:"Water. ",
                    img:require('./images/wat.jpg'),
                    types:[
                        {
                            name:"Water",
                            price:"$0.99"
    
                        }
                       
                          ]
                         }
            }


                ]
    },
    {
        type: 'Side Orders', 
        img:require('./images/sideord.jpg'),
        content:[
            {
                type:"Fries",
                img:require('./images/fries.jpg'),
                details:{
                    description:"French Fries. ",
                    img:require('./images/fries.jpg'),
                    types:[
                        {
                            name:"Small",
                            price:"$2.99"
    
                        },
                        {
                            name:"Large",
                            price:"$4.49"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Cheese Fries",
                img:require('./images/chfries.jpg'),
                details:{
                    description:"French fries covered in cheese. ",
                    img:require('./images/chfries.jpg'),
                    types:[
                        {
                            name:"Small",
                            price:"$3.15"
    
                        },
                        {
                            name:"Large",
                            price:"$3.99"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Pierogies",
                img:require('./images/Pier.jpg'),
                details:{
                    description:"Pierogi are filled dumplings  made by wrapping unleavened dough around a savory or sweet filling and cooking",
                    img:require('./images/Pier.jpg'),
                    types:[
                        {
                            name:"Pierogies",
                            price:"$3.99"
    
                        },
                          ]
                         }
            },
            {
                type:"Garlic Bread",
                img:require('./images/garlicb.jpg'),
                details:{
                    description:"Bread, topped with garlic and olive oil or butter ",
                    img:require('./images/garlicb.jpg'),
                    types:[
                        {
                            name:"Garlic Bread",
                            price:"$2.75"
    
                        },
                          ]
                         }
            },
            {
                type:"Mozzarella Sticks",
                img:require('./images/mozs.jpg'),
                details:{
                    description:"Mozzarella Sticks served with tomato, ketchup, Honey mustard or marinara sauce. ",
                    img:require('./images/mozs.jpg'),
                    types:[
                        {
                            name:"Mozzarella Sticks",
                            price:"$4.99"
    
                        },
                          ]
                         }
            },
            {
                type:"Chicken Fingers",
                img:require('./images/chf.jpg'),
                details:{
                    description:"Chicken Fingers also known as chicken tenders, chicken goujons, chicken strips, tendies or chicken fillets. ",
                    img:require('./images/chf.jpg'),
                    types:[
                        {
                            name:"Chicken Fingers",
                            price:"$4.99"
    
                        },
                          ]
                         }
            },
                ]
    },
    {
        type:'Desserts',
        img:require('./images/dessertt.jpg'),
        content:[
            {
                type:"Baklava",
                img:require('./images/dessert.jpg'),
                details:{
                    description:"Layered Filo Dough, Filled with Crushed Pistachios, with Sweet Honey Syrup. ",
                    img:require('./images/dessert.jpg'),
                    types:[
                        {
                            name:"Baklava",
                            price:"$3.95"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"Ride Pudding/ Sütlaç",
                img:require('./images/sutlac.jpg'),
                details:{
                    description:"Baked & Caramelized Milky Vanilla Pudding. ",
                    img:require('./images/sutlac.jpg'),
                    types:[
                        {
                            name:"Sütlaç",
                            price:"$3.50"
    
                        }
                       
                          ]
                         }
            },
            {
                type:"NY Cheesecake",
                img:require('./images/cake.jpg'),
                details:{
                    description:"A delicious New York Style Cheese Cake. ",
                    img:require('./images/cake.jpg'),
                    types:[
                        {
                            name:"NY Cheesecake",
                            price:"$2.75"
    
                        }
                       
                          ]
                         }
            }
            


        ]

        }
    

];
export default data;